#!/bin/sh

db_host=$1
db_port=$2

while ! nc $db_host $db_port; do
  >&2 echo "Database is unavailable - sleeping"
  sleep 1
done

>&2 echo "Database is up - executing command"

php artisan migrate
php artisan db:seed
php artisan serve --host=0.0.0.0 --port=8080