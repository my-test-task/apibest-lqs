Requirements:
* Deploy Laravel Quick-Start on local computer using single `docker-compose up -d` command;
* Deployment should be available on http://localhost:8080;
* Docker image must only base on vanilla ubuntu or alpine image.

Files description:
* `.env` contains environmental variables that are used in docker-compose.yml by default;
* `lqs.env` contains modified configuration for Laravel Quick-Start;
* `entrypoint.sh` contains script that waits for database container to become available and then proceeds to actual entrypoint.

How to use this:
* `git clone https://gitlab.com/my-test-task/apibest-lqs.git`
* `cd apibest-lqs`
* `docker-compose up -d`

