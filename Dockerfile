FROM ubuntu:22.04
WORKDIR /opt
RUN apt-get update -q
RUN DEBIAN_FRONTEND=noninteractive apt-get install -q -y netcat-openbsd git composer php-curl php-xml php-zip php-mysql npm --no-install-recommends
RUN git clone https://github.com/developervijay7/laravel-quickstart.git
COPY ./lqs.env /opt/laravel-quickstart/.env
COPY ./entrypoint.sh /opt/laravel-quickstart/entrypoint.sh
RUN chmod +x /opt/laravel-quickstart/entrypoint.sh
WORKDIR /opt/laravel-quickstart
RUN composer update --quiet --ignore-platform-reqs
RUN php artisan key:generate
RUN npm ci
RUN npm run dev
 
EXPOSE 8080

CMD [ "./entrypoint.sh", "lqs-mysql", "3306" ]